//
//  Constantes.swift
//  Diccionario
//
//  Created by Javier Yllescas on 11/29/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func redondear () {
        layer.cornerRadius = 10
        clipsToBounds = true
    }
}
extension String {
    public func reemplazarEspacios () -> String {
        return self.replacingOccurrences(of: " ", with: "%20")
    }
}

struct constantes {
    struct apiWikipedia {
        static let url:String = "https://es.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles="
    }
}
