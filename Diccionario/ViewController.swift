//
//  ViewController.swift
//  Diccionario
//
//  Created by Javier Yllescas on 11/29/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbBuscar: UITextField!
    @IBOutlet weak var btnBuscar: UIButton!
    @IBOutlet weak var webResultado: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBuscar.redondear()
    }
    
    @IBAction func buscar(_ sender: Any) {
        let palabra = lbBuscar.text!
        self.buscarPalabra(palabra: palabra)
    }
    
    func buscarPalabra (palabra:String) {
        let url = "\(constantes.apiWikipedia.url)\(palabra.reemplazarEspacios())"
        let objurl = URL(string: url)
        
        let tarea = URLSession.shared.dataTask(with: objurl!){(datos, respuesta, error) in
            if error != nil {
                print(error!)
            }
            else{
                //print(datos!)
                //Consumir JSON
                do{
                    let json = try JSONSerialization.jsonObject(with: datos!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:Any]
                    let subJson = json["query"] as! [String:Any]
                    let subSubJson = subJson["pages"] as! [String:Any]
                    let idConsulta = subSubJson.keys
                    let id = idConsulta.first
                    let idJson = subSubJson[id!] as! [String:Any]
                    let extract = idJson["extract"] as! String
                    
                    DispatchQueue.main.async {
                        self.webResultado.loadHTMLString(extract, baseURL: nil)
                    }
                }catch{
                    print("Error en el JSON")
                }
            }
        }
        tarea.resume()
    }
}

